# 钱包指南

托瑞尔幻想是一个基于BSC链的项目，所以玩家需要创建一个BSC地址，来安装一个 MetaMask 钱包。

MetaMask 是在谷歌浏览器上使用的插件式以太坊钱包。 您只需要在谷歌浏览器中添加相应的扩展程序，更方便。

第一步：下载并安装MetaMask（网址：https://metamask.io/download.html）。 安装完成后浏览器右上角会出现狐狸标志。 只需单击一下，您就可以导入您的钱包或创建一个新钱包。

![](../.gitbook/assets/1.png)

第二步：添加BSC链，由于 MetaMask 默认没有列出 BSC 链，您需要手动添加。 点击 MetaMask 界面顶部的区块链网络，然后选择自定义 RPC。

![](../.gitbook/assets/2.png)

将以下信息一一填写，点击保存：

网络：Binance Smartchain

网址：https://bsc-dataseed.binance.org/

编号：56

符号：BNB

网址：https://www.bscscan.com/

稍后，您可以看到 Binance Smartchain 有一个额外的选项。 您可以将现有的 BSC 帐户私人密钥导入 MetaMask。
